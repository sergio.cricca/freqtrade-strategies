import numpy as np
import freqtrade.vendor.qtpylib.indicators as qtpylib
from freqtrade.strategy import IStrategy, CategoricalParameter, IntParameter, RealParameter
from pandas import DataFrame
from technical.util import resample_to_interval, resampled_merge
from freqtrade.exchange import timeframe_to_prev_date

class SR_strategy(IStrategy):
    
    # Strategy interface parameters
    minimal_roi = {"0": 1, "720": 0.01, "1440": 0}
    stoploss = -0.2  # value to be optimized
    trailing_stop = False

    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        # We will consider a window of the last 200 candles
        window_length = 200

        # Find the rolling minimum price, which will serve as support
        dataframe['rolling_low'] = dataframe['low'].rolling(window_length).min()

        # Find the close price corresponding to the rolling_low, which will serve as the start of the support area
        dataframe['support_start'] = dataframe.loc[dataframe['low'] == dataframe['rolling_low'], 'close']

        # We shift the support start to match the current candle
        dataframe['support_start'] = dataframe['support_start'].shift(periods=-window_length+1)

        # We define the support area as being between the support start price and the rolling low price
        dataframe['support'] = (dataframe['support_start'] + dataframe['rolling_low']) / 2

        # Find the rolling maximum price, which will serve as resistance
        dataframe['rolling_high'] = dataframe['high'].rolling(window_length).max()

        # Find the close price corresponding to the rolling_high, which will serve as the start of the resistance area
        dataframe['resistance_start'] = dataframe.loc[dataframe['high'] == dataframe['rolling_high'], 'close']

        # We shift the resistance start to match the current candle
        dataframe['resistance_start'] = dataframe['resistance_start'].shift(periods=-window_length+1)

        # We define the resistance area as being between the resistance start price and the rolling high price
        dataframe['resistance'] = (dataframe['resistance_start'] + dataframe['rolling_high']) / 2
        
        return dataframe

    def populate_buy_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        dataframe.loc[
            (
                # buy conditions
                (dataframe['close'] <= dataframe['support']) &
                (dataframe['volume'] > 0)
            ),
            'buy'] = 1
        return dataframe

    def populate_sell_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        dataframe.loc[
            (
                # sell conditions
                (dataframe['close'] >= dataframe['resistance']) &
                (dataframe['volume'] > 0)
            ),
            'sell'] = 1
        return dataframe
