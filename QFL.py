# --- Do not remove these libs ---
from freqtrade.strategy import IStrategy
from typing import Dict, List
from functools import reduce
from pandas import DataFrame
import numpy as np


from datetime import datetime, timedelta, timezone
from freqtrade.persistence import Trade
# --------------------------------

import talib.abstract as ta
import freqtrade.vendor.qtpylib.indicators as qtpylib


class QFL(IStrategy):
    """
    Strategy QFL
    author@: Sergio Cricca
    github@: https://github.com/freqtrade/freqtrade-strategies

    How to use it?QFL.py
    > python3 ./freqtrade/main.py -s Strategy001
    """

    # Minimal ROI designed for the strategy.
    # This attribute will be overridden if the config file contains "minimal_roi"
    minimal_roi = {
        "60":  0.01,
        "30":  0.03,
        "20":  0.04,
        "0":  0.05
    }

    # Optimal timeframe for the strategy
    timeframe = '5m'

    # trailing stoploss
    trailing_stop = False
    trailing_stop_positive = 0.01
    trailing_stop_positive_offset = 0.02
    
    stoploss = -0.9

    # run "populate_indicators" only for new candle
    process_only_new_candles = False

    # Experimental settings (configuration will overide these if set)
    use_sell_signal = True
    sell_profit_only = True
    ignore_roi_if_buy_signal = True

    # Optional order type mapping
    order_types = {
        'entry': 'market',
        'exit': 'market',
        'stoploss': 'market',
        'stoploss_on_exchange': False
    }
    
    custom_base = {}
    
    def informative_pairs(self):
        """
        Define additional, informative pair/interval combinations to be cached from the exchange.
        These pair/interval combinations are non-tradeable, unless they are part
        of the whitelist as well.
        For more information, please consult the documentation
        :return: List of tuples in the format (pair, interval)
            Sample: return [("ETH/USDT", "5m"),
                            ("BTC/USDT", "15m"),
                            ]
        """
        return []
    
    def populate_indicators(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        """
        Adds several different TA indicators to the given DataFrame

        Performance Note: For the best performance be frugal on the number of indicators
        you are using. Let uncomment only the indicator you are using in your strategies
        or your hyperopt configuration, otherwise you will waste your memory and CPU usage.
        """

        dataframe["LL"] = ta.MIN(dataframe["low"], timeperiod=28) # find the lowest in timeperiod
        dataframe["confirm"] = ta.MIN(dataframe["low"], timeperiod=7) # check if latest N candles are higher than lowestlow
        dataframe["base"] = dataframe.loc[(dataframe["LL"] == dataframe["confirm"]) & (dataframe["LL"] == dataframe["LL"].shift(-7)), ["LL"]]
        dft = dataframe[['date','base']].copy()
        # dft.dropna(how='any', inplace=True)
        # dft.drop_duplicates(subset=['base'],keep='last', inplace=True)
        dft['cracked'] = np.NaN
        dft['closed'] = np.NaN
        for i in dft.itertuples():
            if np.isnan(i.cracked):
                dft.loc[((dataframe['date'] > i.date) & (dataframe['open'] > i.base) & (dataframe['close'] < i.base)), 'cracked'] = i.base
        dft.loc[dft['cracked'].duplicated(), 'cracked'] = np.nan
        for i in dft.itertuples():
            if np.isnan(i.closed):
                dft.loc[((dataframe['date'] > i.date) & (dataframe['open'] < dataframe['close']) & (dataframe['open'] <= i.cracked) & (dataframe['close'] >= i.cracked)), 'closed'] = i.cracked
        dft.loc[dft['closed'].duplicated(), 'closed'] = np.nan        
        
        
        dataframe = dataframe.join(dft[['cracked', 'closed']], how='outer')
        # dataframe.fillna(method='ffill', inplace=True)
        return dataframe


    def populate_buy_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        """
        Based on TA indicators, populates the buy signal for the given dataframe
        :param dataframe: DataFrame
        :return: DataFrame with buy column
        """
        dataframe.loc[((dataframe['open'] > dataframe['cracked']) & (dataframe['close'] < dataframe['cracked'])), 'buy'] = 1
        
        return dataframe
    
    # def custom_entry_price(self, pair: str, current_time: datetime, proposed_rate: float, entry_tag: Optional[str], **kwargs) -> float:
    #     dataframe, last_updated = self.dp.get_analyzed_dataframe(pair=pair,timeframe=self.timeframe)
    #     new_entryprice = dataframe['base'].iat[-1]

    #     return new_entryprice

    # def custom_exit_price(self, pair: str, trade: Trade, current_time: datetime, proposed_rate: float, current_profit: float, **kwargs) -> float:
    #     dataframe, _ = self.dp.get_analyzed_dataframe(pair=pair, timeframe=self.timeframe)
    #     # get the current candle
    #     current_candle = dataframe.iloc[-1].squeeze()        
        
    #     if current_candle['high'] >= current_candle['base_closed']:
    #         return 'sell_at_base'

    #     return None



    def populate_sell_trend(self, dataframe: DataFrame, metadata: dict) -> DataFrame:
        """
        Based on TA indicators, populates the sell signal for the given dataframe
        :param dataframe: DataFrame
        :return: DataFrame with buy column
        """
        dataframe.loc[(dataframe['open'] < dataframe['close']) & (dataframe['open'] < dataframe['closed']) & (dataframe['close'] > dataframe['closed']), 'sell'] = 1
        return dataframe
        
        